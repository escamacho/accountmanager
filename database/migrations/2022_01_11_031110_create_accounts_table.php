<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('user_id')->unsigned();
            $table->integer('debtor_id')->unsigned();
            $table->integer('bank_account_id')->unsigned();
            $table->string('note');
            $table->enum('status', ['Created', 'Send', 'Paid'])->default('Created');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('debtor_id')->references('id')->on('debtors');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}