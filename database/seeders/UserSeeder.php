<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::Create([
            'name' => 'Eva Sandrith Camacho Romero',
            'email' => 'camachoeva.007@gmail.com',
            'password' => bcrypt('admin'),
            'nit' => '1003376884',
        ]);
    }
}