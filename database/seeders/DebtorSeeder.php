<?php

namespace Database\Seeders;

use App\Models\Debtor;
use Illuminate\Database\Seeder;

class DebtorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // SMA
        $debtor = new Debtor();
        $debtor->business_name = "SMA Ingenieria";
        $debtor->nit = "900552585";
        $debtor->phone = "3052882363";
        $debtor->address = "Pereira";
        $debtor->user_id =1;
        $debtor->save();
    }
}
