<?php

namespace Database\Seeders;


use App\Models\BankAccount;
use Illuminate\Database\Seeder;

class BankAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Banco Caja Social
        $bank_account = new BankAccount();
        $bank_account->account_type = "Ahorros";
        $bank_account->number = "24080534536";
        $bank_account->bank = "Banco Caja Social";
        $bank_account->user_id = 1;
        $bank_account->save();

        // Bancolombia
        $bank_account = new BankAccount();
        $bank_account->account_type = "Ahorros";
        $bank_account->number = "52421006989";
        $bank_account->bank = "Bancolombia";
        $bank_account->user_id = 1;
        $bank_account->save();
        
        // Nequi
        $bank_account = new BankAccount();
        $bank_account->account_type = "Ahorros";
        $bank_account->number = "3022094664";
        $bank_account->bank = "Nequi";
        $bank_account->user_id = 1;
        $bank_account->save();

       
    }
}