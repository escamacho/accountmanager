<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('maquinas/get-all', function () {
    return [
        "maquinas" => [
            [
                "serie" => "123450",
                "nombre" => "maquina 1",
            ],
            [
                "serie" => "456780",
                "nombre" => "89789"
            ]
        ]
    ];
});


Route::get('operadores/get-all', function () {
    return [
        "operadores" => [
            [
                "id" => "123450",
                "nombre" => "operador 1",
            ],
            [
                "id" => "456780",
                "nombre" => "89789"
            ]
        ]
    ];
});


Route::get('frentes/get-all', function () {
    return [
        "frentes" => [
            [
                "id" => "123450",
                "nombre" => "Frente 1",
            ],
            [
                "id" => "456780",
                "nombre" => "89789"
            ]
        ]
    ];
});

Route::get('causas/get-all', function () {
    return [
        "causas" => [
            [
                "id" => "123450",
                "nombre" => "Causa 1",
            ],
            [
                "id" => "456780",
                "nombre" => "89789"
            ]
        ]
    ];
});

Route::get('sistemas_intervenidos/get-all', function () {
    return [
        "sistemas_intervenidos" => [
            [
                "id" => "123450",
                "nombre" => "Sistemas Intervenido 1",
            ],
            [
                "id" => "456780",
                "nombre" => "89789"
            ]
        ]
    ];
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
