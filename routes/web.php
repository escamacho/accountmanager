<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DebtorController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\BankAccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});




Route::get('/offline', function () {
    return view('vendor.laravelpwa.offline');
});

Auth::routes();
Route::prefix("/accounts")->group(function () {
    Route::get('/', [AccountController::class, 'index'])->name('accounts.index');
    Route::get('/add', [AccountController::class, 'create'])->name('accounts.add');
    Route::post('/add', [AccountController::class, 'store'])->name('accounts.add');
    Route::get('/edit/{account}', [AccountController::class, 'edit'])->name('accounts.edit');
    Route::post('/edit/{account}', [AccountController::class, 'update'])->name('accounts.edit');
    Route::get('/accounts/print/account/{account}', [AccountController::class, 'print'])->name('accounts.print');
    Route::get('/accounts/send/account/{account}', [AccountController::class, 'send'])->name('accounts.send');
});

Route::prefix("/debtors")->group(function () {
    Route::get('/', [DebtorController::class, 'index'])->name('debtors.index');
    Route::post('add', [DebtorController::class, 'store'])->name('debtors.add-debtor');
    Route::post('edit', [DebtorController::class, 'update'])->name('debtors.edit-debtor');
});

Route::prefix("/bank_accounts")->group(function () {
    Route::get('/', [BankAccountController::class, 'index'])->name('bank_accounts.index');
    Route::post('add', [BankAccountController::class, 'store'])->name('bank_accounts.add');
    Route::post('edit', [BankAccountController::class, 'update'])->name('bank_accounts.edit');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout',  [App\Http\Controllers\Auth\LoginController::class, 'logout']);