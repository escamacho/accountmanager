@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/debtors.css') }}">
@endpush

@section('content')
    <section>
        <div class="row align-self-end">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <button id="new_debtor" class="btn btn-success btn-block text-center">
                    <i class="fas fa-plus-circle"></i> Crear Nuevo Cliente
                </button>
            </div>
        </div>
    </section>
    <section>

        <div class="row">
            <div class="col-12">
                <table id="table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th scope="col">Nit</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Dirección</th>
                            <th class="text-center" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($debtors as $debtor)
                            <tr>
                                <td>{{ $debtor->nit }}</td>
                                <td>{{ $debtor->business_name }}</td>
                                <td>{{ $debtor->email }}</td>
                                <td>{{ $debtor->phone }}</td>
                                <td>{{ $debtor->address }}</td>
                                <td class="text-center">
                                    <a title="Editar" class="icon icon-edit" id='edit_debtor_action'
                                        data-id="{{ $debtor->id }}" data-nit="{{ $debtor->nit }}" data-business_name="{{ $debtor->business_name }}"
                                        data-email="{{ $debtor->email }}" data-phone="{{ $debtor->phone }}"
                                        data-address="{{ $debtor->address }}" data-toggle="modal" data-target="#editDebtor">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
@push('modals')
    @include('debtors.modals.add_debtor');
    @include('debtors.modals.edit_debtor');
@endpush
@section('scripts')
    <script src="{{ asset('js/debtors.js') }}"></script>
@stop
