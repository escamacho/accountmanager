<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AccountManager</title>
    <!-- Fonts -->
    <link rel="stylesheet"
        href="https://unpkg.com/@fortawesome/fontawesome-free@5.14.0/css/all.min.css"
        crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&family=Inter:wght@300;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" crossorigin="anonymous">

    <!-- Responsive-->

    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">

    <script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
    @stack('styles')
    @laravelPWA
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-white header">
        <h2><a class="web-site-title" href="/">AccountManager</a></h2>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @if (Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('accounts.index') }}"
                            class="elementor-item">Cuentas
                            de Cobro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('debtors.index') }}"
                            class="elementor-item">Deudores/Clientes</a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('bank_accounts.index') }}"
                            class="elementor-item">Cuentas bancarias</a>
                    </li>
                @else
                <li>
                    <a class="nav-link" href="/login"
                        class="elementor-item">Login</a>
                </li> @endif
            </ul>
 
          <form class="form-inline
        my-2 my-lg-0">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="/logout" class="elementor-item">Salir</a>
        </li>
    </ul>
    </form>
    </div>
    </nav>

    <main>
        <div id="app">
            <div class="container">
                @include('layouts.flash_messages')
            </div>
            @yield('content')
        </div>
    </main>
    @yield('modal')
    @stack('modals')

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf8" src="/DataTables/datatables.js"></script>


    @yield('scripts')
    @stack('scripts')
    </body>

</html>
