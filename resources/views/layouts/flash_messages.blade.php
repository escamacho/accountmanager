@if (Session::has('flash_message'))
    <div style="margin:20px auto;" class="alert {{Session::has('flash_alert_type') ? 'alert-'.session('flash_alert_type') : '' }} {{Session::has('flash_message_important') ? 'alert-important' : '' }}">
        @if(Session::has('flash_message_important'))
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @endif
        {{session('flash_message')}}
    </div>
@endif
@if (!empty($errors->all()))
    <div style="margin:20px auto;" class="alert alert-dismissible fade show alert-danger">
       
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       
        @foreach($errors->all() as $message)
            {{$message}}
        @endforeach
    </div>
@endif