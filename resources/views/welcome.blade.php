@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
@endpush

@section('content')
    <section class="text-center">
        <h5>EN ESTE SITIO PUEDES GENERAR TUS CUENTAS DE COBRO COMO INDEPENDIENTE O EMPRESA</h5>

        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <img class="dots mb-4" width="90%" height="90%" src="{{ asset('/images/invoice.jpg') }}" alt="dots"
                    data-wow-duration="1.3s" data-wow-delay="0.5s" />
            </div>
        </div>

        <br>
        <span>Este es un producto de <a href="http://qesoft.com.co/">QE SOFT</a></span>
    </section>
@endsection
@section('scripts')

@stop
