@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/bank_accounts.css') }}">
@endpush

@section('content')
        <section>
            <div class="row align-self-end">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <button id="new_bank_account" class="btn btn-success btn-block text-center"> 
                        <i class="fas fa-plus-circle"></i> Crear Nueva Cuenta Bancaria
                    </button>
                </div>
            </div>
        </section>
<section>
    
    <div class="row">
        <div class="col-12">
            <table id="table" class="table table-striped table-bordered" style="width:100%" >
                <thead>
                    <tr>
                        <th scope="col">Tipo de Cuenta</th>
                        <th scope="col">Número de Cuenta</th>
                        <th scope="col">Banco</th>
                        <th class="text-center" scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                   
                        @foreach ($accounts as $account)
                            <tr>
                                <td>{{ $account->account_type }}</td>
                            <td>{{ $account->number }}</td>
                            <td>{{ $account->bank }}</td>
                            <td class="text-center">
                                <a title="Editar" class="icon icon-edit"
                                 id='edit_bank_account_action' 
                                data-id="{{$account->id}}"
                                data-account_type="{{$account->account_type}}"
                                data-number="{{$account->number}}"
                                data-bank="{{$account->bank}}"
                                data-url="{{$account->url}}"
                                data-toggle="modal" data-target="#editBankAccount">
                                    <i class="fas fa-edit"></i>
                                </a>
    
                                <a title="Ver PDF" class="icon icon-see-pdf" href="{{$account->url}}" target="blank"><i
                                        class="fas fa-file-pdf"></i></a>
                            </td>
                            </tr>
                        @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</section>
    
@endsection
@push('modals')
    @include('bank_accounts.modals.add_bank_account');
    @include('bank_accounts.modals.edit_bank_account');
@endpush
@section('scripts')
    <script src="{{ asset('js/bank_accounts.js') }}"></script>
@stop
