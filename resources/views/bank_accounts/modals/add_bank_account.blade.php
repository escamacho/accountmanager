<div class="modal fade" id="addBankAccount" tabindex="-1" aria-labelledby="addBankAccountLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addBankAccountLabel">Agregar Cuenta Bancaria</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('bank_accounts.add') }}" method="POST">
                @csrf
                <div class="modal-body" id="">
                    <div class="form-group">
                        <label for="account_type">Tipo de cuenta</label>
                        <select class="form-control form-select" name="account_type" id="account_type"
                            aria-label="Seleccione el tipo de cuenta" required>
                            <option value="Ahorros" selected>Ahorros</option>
                            <option value="Corriente">Cuenta Corriente</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="number">Numero</label>
                        <input type="text" class="form-control" name="number" id="number" required>
                    </div>
                    <div class="form-group">
                        <label for="bank">Banco</label>
                        <input type="text" class="form-control" name="bank" id="bank" required>
                    </div>
                    <div class="form-group">
                        <label for="url">URL del certificado de cuenta</label>
                        <input type="url" class="form-control" name="url" id="url" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
