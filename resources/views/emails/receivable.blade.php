<div style="font-family: arial; color:black">
    <strong>Hola!</strong>
    <br><br>
    Adjunto cuenta de cobro asociada a la fecha: {{ $now }}
    <br><br>
    Certificación bancaria: <a href="{{ $account->bankAccount->url }}">Certificación Bancaria</a>
    <br><br>
    Gracias,<br><br>
    <strong>Eva Sandrith Camacho Romero</strong>
</div>
