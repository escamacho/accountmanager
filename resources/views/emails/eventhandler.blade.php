@component('mail::message')
<div style="font-family: arial;">

<div>
    <div>Email: {{$event['email']}}</div>
    <div>Category: {{$event['category']}}</div>
    <div>Event: {{$event['event']}}</div>
    <div>Time: {{date('Y-m-d',($event['timestamp']))}}</div>
</div>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
</div>