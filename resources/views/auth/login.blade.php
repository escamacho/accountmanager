@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">
@endpush

@section('content')
<div class="container">
   <div class="row justify-content-center">
    <div class="col-10 col-sm-10 col-md-6 col-lg-5 col-xl-5">
        <div class="card">
            <div class="card--header text-center">        
                <h5>¡Bienvenido!</h5>
                <h4>Ingresa a Nuestra Comunidad</h4>
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email" class="col-form-label">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-form-label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                 
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-auth btn-block">
                            {{ __('Login') }}
                        </button>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </form>
                <br>
                <hr>
                <p class="text-center">¿No tienes una Cuenta? <a href="/register">Registrate</a></p>
            </div>
        </div>
    </div>
   </div>
</div>


@endsection
