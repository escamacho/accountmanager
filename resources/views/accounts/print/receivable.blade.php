<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Receivable</title>
    <style>
        html {
            min-height: 100%;
            position: relative;
            font-size: 62.5%;
            font-family: "DM Sans", sans-serif;
        }

        body {
            margin: 0;
            margin-bottom: 40px;
        }

        .text-center {
            text-align: center;
        }

        .information-debtor {
            margin-bottom: 100px;
        }

        .user-information,
        .account-information {
            margin-bottom: 80px;
        }

        .debtor-name {
            font-size: 1.5rem;
            line-height: 0.1rem;
            font-weight: bold;
        }

        .title-footer {
            text-align: center;
            font-size: 8px;
        }

        .title-footer a {
            text-decoration: none;
        }

        .receivable-title {
            font-size: 1.3rem;
            line-height: 0.1rem;
            font-weight: bold;
        }

        .receivable-data {
            font-size: 1.3rem;
            line-height: 0.1rem;
            font-weight: normal;
        }

        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .pago {
            margin: 0px 10% 0px 10%;
        }

        .title-table {
            text-align: center;
            font-weight: bold;
        }

        .footer-pdf {
            position: absolute;
            bottom: 40px;
            width: 100%;
            height: 120px;
        }


        #img-firm {
            margin: 0px;
            padding: 0px;
            width: 150px;
            height: 45px;
        }

        #line-firm {
            margin: 0px;
            padding: 0px;
        }

        .footer-pdf h6 {
            margin: 0px;
            padding-top: 15px;
        }
    </style>

</head>

<body>
    <section class="text-center header mt-3">
        <h1>CUENTA DE COBRO</h1>
    </section>
    <section class="text-center content-pdf">
        <section class="information-debtor">
            <h6 class="debtor-name">{{ $data['account']->debtor->business_name }}</h6>
            <h6 class="receivable-data">NIT: {{ $data['account']->debtor->nit }}</h6>
        </section>

        <section class="user-information">
            <h6 class="receivable-title">DEBE A:</h6>
            <h6 class="receivable-data">{{ $data['account']->user->name }}</h6>
            <h6 class="receivable-data">C.C. {{ $data['account']->user->nit }}</h6>
        </section>


        <section class="account-information">
            <h6 class="receivable-title">LA SUMA DE:</h6>
            <h6 class="receivable-data">${{ $data['account']->total_account_amount }}
                ({{ $data['account']->total_account_amount_in_letters }})</h6>
        </section>


        <section class="items-information">
            <h6 class="receivable-title">POR CONCEPTO DE:</h6>
            @foreach ($data['account']->items as $item)
                <h6 class="receivable-data">{{ $item->description }}</h6>
            @endforeach
        </section>

        <section class="items-information">
            <h6 class="receivable-title">DONDE PAGAR</h6>

            <table class="pago" width="80%">
                <thead>
                    <tr>

                        <td class="title-table">Número de cuenta bancaria</td>

                        <td class="title-table">Link de pago</td>

                        <td class="title-table">Qr de pago</td>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $data['bank_account']->number . ' (Tipo de cuenta: ' . $data['bank_account']->account_type . ' ' . $data['bank_account']->bank . ' )' }}
                        </td>
                        <td>{{ $data['account']->link_pay }}</td>
                        <td>NA</td>
                    </tr>
                </tbody>
            </table>
        </section>
    </section>

    <section class="footer-pdf">
        <h6 class="receivable-title">Cordialmente,</h6>
        <br><br><br>

        <div class="firm">
            <img id="img-firm" src="https://s3.us-east-2.amazonaws.com/public.personal/Firma.jpeg" alt="firm">
            <h6 id="line-firm">__________________________________________________</h6>
        </div>
        <h6 class="receivable-data">{{ $data['account']->user->name }}</h6>
        <h6 class="receivable-data">C.C. {{ $data['account']->user->nit }}</h6>
        <h6 class="receivable-data">Valledupar, {{ Date('d') . ' de ' . Date('M') . ' del ' . Date('Y') }}</h6>
        <br><br>
        <hr>
        <div class="text-center">
            <span class="title-footer">Este es un producto de <a href="http://qesoft.com.co/">QE SOFT</a></span>
        </div>
    </section>


</body>

</html>
