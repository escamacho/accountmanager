@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accounts.css') }}">
@endpush

@section('content')
<div class="container">
    <section>
        <div class="row align-self-end">
            <div class="col-12 col-sm-12 col-md-12 col-lg-2">
                <a class="btn btn-success btn-block text-center" href="{{ route('accounts.add') }}">
                    <i class="fas fa-plus-circle"></i> Crear Nueva Cuenta
                </a>
            </div>
        </div>
    </section>
    <section>

        <div class="row">
            <div class="col-12">
                <table id="table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Banco</th>
                            <th scope="col">Nota</th>
                            <th scope="col">Total</th>
                            <th class="text-center" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($accounts as $account)
                            <tr>
                                <td>{{ $account->id }}</td>
                                <td>{{ $account->date_format_long_letter }}</td>
                                <td><a href="{{$account->bankAccount->url}}" target="_blank">{{$account->bankAccount->bank}}</a></td>
                                <td>{{ $account->note }}</td>
                                <td>{{ $account->total_account_amount }}</td>
                                <td class="text-center">
                                    <a title="Editar" class="icon icon-edit" href="{{ route('accounts.edit',[$account->id]) }}">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a title="Enviar por Email" class="icon icon-send"
                                        href="{{ route('accounts.send', ['account' => $account->id]) }}"><i
                                            class="fas fa-paper-plane"></i></a>

                                    <a title="Ver PDF" class="icon icon-see-pdf"
                                        href="{{ route('accounts.print', ['account' => $account->id]) }}"
                                        target="blank"><i class="fas fa-file-pdf"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>


@endsection
@section('scripts')
    <script src="{{ asset('js/accounts.js') }}"></script>
@stop
