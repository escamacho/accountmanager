@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accounts.css') }}">
@endpush

@section('content')
    <section>

        <div class="row">
            <div class="col-12">
                <div class="card shadow-sm">
                    <form method="POST" action="{{ route('accounts.edit', [$account]) }}" id="edit-account" name="edit-account">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="debtor">Cliente</label>
                                <select class="form-control form-select" name="debtor" id="debtor" aria-label=""
                                    required>
                                    @foreach ($debtors as $debtor)
                                        <option value="{{ $debtor->id }}"
                                            {{ $account->debtor_id == $debtor->id ? 'selected="selected"' : '' }}>
                                            {{ $debtor->business_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="debtor">Cuenta Bancaria</label>
                                <select class="form-control form-select" name="bank_account" id="bank_account"
                                    aria-label="" required>
                                    @foreach ($bank_accounts as $bank_account)
                                        <option value="{{ $bank_account->id }}"
                                            {{ $account->bank_account_id == $bank_account->id ? 'selected="selected"' : '' }}>
                                            {{ $bank_account->bank }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="note">Note</label>
                                <textarea class="form-control" id="note" name="note" rows="5" required>{{ $account->note }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <textarea class="form-control" id="description" name="description" rows="5" required>{{ $item->description }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="note">Costo</label>
                                <input class="form-control" type="number" id="amount" name="amount"
                                    value="{{ $item->amount }}" required>
                            </div>

                            <div class="form-group">
                                <label for="note">Link del Pago</label>
                                <input class="form-control" type="url" id="link_pay" name="link_pay" value="{{ $account->link_pay }}" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Actualizar Cuenta/Factura</button>

                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script src="{{ asset('js/accounts.js') }}"></script>
@stop
