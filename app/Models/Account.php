<?php

namespace App\Models;

use NumberFormatter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Account extends Model
{
    use HasFactory;
    protected $table = 'accounts';
    protected $fillable = [
        'user_id',
        'account_id',
        'bank_account_id',
        'note',
        'amount',
        'link_pay'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
	}

    public function debtor(){
        return $this->belongsTo('App\Models\Debtor');
	}

    public function bankAccount(){
        return $this->belongsTo('App\Models\BankAccount');
	}

    public function items(){
        return $this->hasMany('App\Models\AccountItem','account_id');
    }

    public function getTotalAccountAmountInLettersAttribute(){
        $amount = new NumberFormatter("es", NumberFormatter::SPELLOUT);
        $totals = $this->items()->sum('amount');
        $type_currency = $totals > 999999 ? ' de pesos' : ' pesos';
        return ucfirst($amount->format($totals) .$type_currency);
    }

    public function getTotalAccountAmountAttribute(){
        return $this->items()->sum('amount');
    }

    public function getDateFormatLongLetterAttribute(){
        return date('M j, Y', strtotime($this->created_at)) ;
    }
}