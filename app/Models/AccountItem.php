<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountItem extends Model
{
    use HasFactory;
    protected $table = 'account_items';
    protected $fillable = [
        'note',
    ];

    public function account(){		
        return $this->belongsTo('App\Models\Account', 'account_id', 'id');
	}
}