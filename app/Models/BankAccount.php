<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    use HasFactory;

    protected $table = 'bank_accounts';
    protected $fillable = [
        'account_type',
        'number',
        'bank',
        'user_id',
        'url',
    ];

    public function user(){		
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
	}
}