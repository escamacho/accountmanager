<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Debtor extends Model
{
    use HasFactory;
    protected $table = 'debtors';
    protected $fillable = [
        'business_name',
        'nit',
        'phone',
        'address',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
	}
    
    public function accounts(){
        return $this->hasMany('App\Models\Account');
    }
}