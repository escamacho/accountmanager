<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Model implements AuthenticatableContract
{
    use HasFactory;
    use Authenticatable;
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function banks_accounts(){		
        return $this->hasMany('App\Models\BankAccount','user_id');
	}
}