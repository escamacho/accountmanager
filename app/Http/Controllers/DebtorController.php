<?php

namespace App\Http\Controllers;

use App\Models\Debtor;
use Illuminate\Http\Request;

class DebtorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $debtors = Debtor::where('user_id',auth()->id())->get();
       return view('debtors.index', compact('debtors'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $debtor = new Debtor();
        $debtor->business_name = $request->business_name;
        $debtor->nit = $request->nit;
        $debtor->phone = $request->phone;
        $debtor->address = $request->address;
        $debtor->user_id =  auth()->id();
        $debtor->save();
        
        return back()->with([
            'flash_message' => 'Cliente Creado!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Debtor  $debtor
     * @return \Illuminate\Http\Response
     */
    public function show(Debtor $debtor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Debtor  $debtor
     * @return \Illuminate\Http\Response
     */
    public function edit(Debtor $debtor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Debtor::where('id', $request->id )
                    ->update([
                        'business_name' => $request->business_name,
                        'nit' => $request->nit,
                        'phone' => $request->phone,
                        'email' => $request->email,
                        'address' => $request->address,
                    ]);

      
        return back()->with([
            'flash_message' => 'Cliente Actualizado!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Debtor  $debtor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Debtor $debtor)
    {
        //
    }


}