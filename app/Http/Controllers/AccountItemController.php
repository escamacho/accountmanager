<?php

namespace App\Http\Controllers;

use App\Models\AccountItem;
use Illuminate\Http\Request;

class AccountItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccountItem  $accountItem
     * @return \Illuminate\Http\Response
     */
    public function show(AccountItem $accountItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccountItem  $accountItem
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountItem $accountItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountItem  $accountItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountItem $accountItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountItem  $accountItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountItem $accountItem)
    {
        //
    }
}