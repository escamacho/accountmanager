<?php

namespace App\Http\Controllers;

use App\Models\Debtor;
use App\Models\Account;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Services\ScheduledEmailService;
use App\Models\AccountItem;
use App\Models\BankAccount;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::where('user_id', auth()->id())->get();
        return view('accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bank_accounts = BankAccount::where('user_id', auth()->id())->get();
        $debtors = Debtor::where('user_id', auth()->id())->get();
        return view('accounts.add_account', compact('debtors', 'bank_accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $account = new Account();
        $account->user_id = auth()->id();
        $account->debtor_id = $request->debtor;
        $account->bank_account_id = $request->bank_account;
        $account->note = $request->note;
        $account->link_pay = $request->link_pay;
        $account->save();


        $account_item = new AccountItem();
        $account_item->account_id = $account->id;
        $account_item->description = $request->description;
        $account_item->amount = $request->amount;
        $account_item->save();


        return redirect('/accounts')->with([
            'flash_message' => 'Cuenta/Factura creada!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {

        $bank_accounts = BankAccount::where('user_id', auth()->id())->get();
        $debtors = Debtor::where('user_id', auth()->id())->get();
        $item = AccountItem::where('account_id', $account->id)->first();
        return view('accounts.edit_account', compact('account', 'item', 'debtors', 'bank_accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
     
        Account::where('id', $account->id)
            ->update([
                'debtor_id' => $request->debtor,
                'bank_account_id' => $request->bank_account,
                'note' => $request->note,
                'link_pay' => $request->link_pay
            ]);

            AccountItem::where('account_id', $account->id)
            ->update([
                'description' => $request->description,
                'amount' => $request->amount,
            ]);

    
        return back()->with([
            'flash_message' => 'Cuenta/Factura Actualizada!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }

    public function print(Account $account)
    {

        $data = [
            "account" => $account,
            "bank_account" => $account->bankAccount
        ];

        $pdf = \PDF::loadView('accounts.print.receivable', compact('data'));
        return $pdf->stream('Receivable.pdf');
    }

    public function send(Account $account, ScheduledEmailService $email_handler)
    {
        $data = [
            "account" => $account,
            "bank_account" => $account->bankAccount
        ];

        $pdf = \PDF::loadView('accounts.print.receivable', compact('data'));

        $email_handler->sendEmailReceivable($account, $pdf->output());
        return redirect()->back();
    }
}