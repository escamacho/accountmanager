<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankAccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
        $accounts = BankAccount::where('user_id',auth()->id())->get();
        return view('bank_accounts.index', compact('accounts'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank_account = new BankAccount();
        $bank_account->account_type = $request->account_type;
        $bank_account->number = $request->number;
        $bank_account->bank = $request->bank;
        $bank_account->url = $request->url;
        $bank_account->user_id = auth()->id();

        $bank_account->save();

        return back()->with([
            'flash_message' => 'Cuenta bancaria guardada!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccount $bankAccount)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        BankAccount::where('id', $request->id )
                    ->update([
                        'account_type' => $request->account_type,
                        'number' => $request->number,
                        'bank' => $request->bank,
                        'url' => $request->url,
                    ]);

      
        return back()->with([
            'flash_message' => 'Cuenta bancaria actualizada!',
            'flash_message_important' => true,
            'flash_alert_type' => "success",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccount $bankAccount)
    {
        //
    }
}