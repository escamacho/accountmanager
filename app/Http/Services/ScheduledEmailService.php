<?php

namespace App\Http\Services;


use App\Models\Account;
use App\Mail\Receivable;
use Illuminate\Support\Facades\Mail;

class ScheduledEmailService
{
    public function sendEmailReceivable(Account $account,$pdf){

		Mail::to('camachoeva.07@gmail.com')->send(new Receivable($account,$pdf));
		return true;
	}
}