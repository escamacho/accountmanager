<?php

namespace App\Mail;

use App\Models\Account;
use Barryvdh\DomPDF\PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Queue\ShouldQueue;

class Receivable extends Mailable
{
    use Queueable, SerializesModels;
    public $account;
    public $pdf;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Account $account,$pdf)
    {

        $this->account = $account;
        $this->pdf = $pdf;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $now = Date("d-m-Y");
        $account = $this->account;

        return $this->cc('camachoeva.07@gmail.com')->subject('Cuenta de Cobro de ' .$this->account->user->name)
                    ->attachData($this->pdf, "Cuenta de cobro ".$now ." .pdf")
                    ->markdown('emails.receivable',compact('account','now'));

    }
}