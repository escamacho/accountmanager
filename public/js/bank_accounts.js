$(document).ready(function () {
    $("#table").DataTable();

    $("#new_bank_account").click(function () {
        $("#addBankAccount").modal("show");
    });

    $("#editBankAccount").on("show.bs.modal", function (e) {
        var id = $(e.relatedTarget).data("id");
        var number = $(e.relatedTarget).data("number");
        var account_type = $(e.relatedTarget).data("account_type");
        var bank = $(e.relatedTarget).data("bank");
        var url = $(e.relatedTarget).data("url");

        $(this).find("#id").val(id);
        $(this).find("#account_type").val(account_type);
        $(this).find("#number").val(number);
        $(this).find("#bank").val(bank);
        $(this).find("#url").val(url);

    });
});
