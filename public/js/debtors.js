$(document).ready(function () {
    $("#table").DataTable();

    $("#new_debtor").click(function () {
        $("#addDebtor").modal("show");
    });

    $("#editDebtor").on("show.bs.modal", function (e) {
        var id = $(e.relatedTarget).data("id");
        var business_name = $(e.relatedTarget).data("business_name");
        var nit = $(e.relatedTarget).data("nit");
        var email = $(e.relatedTarget).data("email");
        var phone = $(e.relatedTarget).data("phone");
        var address = $(e.relatedTarget).data("address");

        $(this).find("#id").val(id);
        $(this).find("#business_name").val(business_name);
        $(this).find("#nit").val(nit);
        $(this).find("#email").val(email);
        $(this).find("#phone").val(phone);
        $(this).find("#address").val(address);
    });
});
